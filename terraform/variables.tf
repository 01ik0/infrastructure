variable "yc_cloud" {
  type = string
  description = "Yandex Cloud ID"
}

variable "yc_folder" {
  type = string
  description = "Yandex Cloud folder"
}

variable "yc_token" {
  type = string
  description = "Yandex Cloud token"
}

variable "image_id" {
  type    = string
  default = "fd81u2vhv3mc49l1ccbb"
}

variable "subnet_id" {
  type    = string
  default = "e9buqdtk932abpsvrje1"
}
