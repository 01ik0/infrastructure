# List of indexes to be created to optimize the database query

```
CREATE INDEX idx_order_id ON public.order_product USING btree (order_id, product_id);
CREATE INDEX idx_o_id ON public.orders USING btree (id);
CREATE INDEX idx_p_id ON public.product USING btree (id);
```
